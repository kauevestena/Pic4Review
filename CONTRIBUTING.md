# Contributing

There are several ways to contribute to the project, here are a few ones.


## Translate the application

Don't worry, this one is easy ! We use Transifex, which is a simple platform for translating applications. We have all the [labels there](https://www.transifex.com/openlevelup/pic4review/), in English, so you can write translations into your language. Let us know when you're done, that way we will update the application to make your language available.

If you run into any issue for doing this (like your language isn't available in the list), please [contact us](mailto:panieravide@riseup.net) or [open an issue](https://framagit.org/Pic4Carto/Pic4Review/issues/new).


## Solve issues/develop features

If you are a developer, you can see the [list of open issues](https://framagit.org/Pic4Carto/Pic4Review/issues) and start working on one of them. To start development, please read [development documentation](DEVELOP.md).
